## Jacob Deitloff
## 10. April 2013
## Python 3
##
## Program designed, based on the algorithm described in ITC, Edition 2, to
## convert an NFA into a DFA. All code was self-written, and is made freely
## available to the public at:
##      http://jacob.deitloff.com/code/convertNfaToDfa.py

#### DO NOT MODIFY THIS CODE. SCROLL TO THE BOTTOM OF THIS FILE. ####
import itertools

EPSILON = "ε"

class DeltaEntry:
    def __init__(self, q, a):
        self.q = q
        self.a = a

    def getQ(self):
        return self.q

    def getA(self):
        return self.a

    def __str__(self):
        return "δ(%s, %s)" % (self.q, self.a)

    def __eq__(self, other):
        if not isinstance(other, DeltaEntry):
            return False
        return (self.q == other.q and self.a == other.a)

    def __hash__(self):
        return hash("%sδ%s" % (self.q, self.a))

def formatQ(q):
    if len(q) == 0:
        qStr = "∅"
    else:
        qStr = "{"
        firstElement = True
        for qElement in q:
            if firstElement:
                firstElement = False
            else:
                qStr += ", "
            qStr += str(qElement)
        qStr += "}"
    return qStr

def printFa(Q, sigma, delta, q0, F):
    # Print Q
    print("Q =\t", end="")
    if len(Q) > 0:
        print("{", end="")
        firstQ = True
        for q in Q:
            if firstQ:
                firstQ = False
            else:
                print(",", end="")
            print(" ", end="")
            print(formatQ(q), end="")
        print(" }")
    else:
        print("∅")

    # Print Sigma
    print("Σ =\t", end="")
    if len(sigma) > 0:
        print("{", end="")
        firstSigma = True
        for s in sigma:
            if firstSigma:
                firstSigma = False
            else:
                print(",", end="")
            print(" ", end="")
            print(s, end="")
        print(" }")
    else:
        print("∅")

    # Print Delta
    print("δ =\t", end="")
    if len(delta) > 0:
        print("{", end="")
        firstDelta = True
        maxDeltaQLength = 0
        for d in delta:
            if len(formatQ(d.getQ())) > maxDeltaQLength:
                maxDeltaQLength = len(formatQ(d.getQ()))
        dIndex = 0;
        for d in delta:
            deltaStr = "( " + (("%%%ds" % maxDeltaQLength) % formatQ(d.getQ())) + ", " + \
                       d.getA() + " )  =>  " + formatQ(delta[d])
            dIndex += 1
            if dIndex == len(delta):
                deltaStr += "\t\t}"
            if firstDelta:
                print("\t" + deltaStr)
                firstDelta = False
            else:
                print("\t\t" + deltaStr)
    else:
        print("∅")

    # Print q0
    print("q0 =\t", formatQ(q0), sep='')

    # Print F
    print("F = \t", end="")
    if len(F) > 0:
        print("{", end="")
        firstF = True
        for f in F:
            if firstF:
                firstF = False
            else:
                print(",", end="")
            print(" ", end="")
            print(formatQ(f), end="")
        print(" }")
    else:
        print("∅")

def E(r, delta):
    global EPSILON
    destinations = [ r ]
    if DeltaEntry(r, EPSILON) in delta:
        destinations.extend(delta[DeltaEntry(r, EPSILON)])
    return destinations

def convertToDfa(Q, sigma, delta, q0, F):
    global EPSILON

    # print what we have to start with
    print("----------------  NFA  ------------------")
    printFa(Q, sigma, delta, q0, F)
    
    # generate QPrime
    QPrime = [ [] ]
    for r in range(len(Q)):
        newCombos = list(itertools.combinations(Q, r + 1))
        for newCombo in newCombos:
            newCombo = list(newCombo)
            newCombo.sort()
            QPrime.append(newCombo)

    # generate q0Prime
    q0Prime = E(q0, delta)
    q0Prime.sort()

    # generate FPrime
    FPrime = [ ]
    for qPrime in QPrime:
        for qPrimeComponent in qPrime:
            if qPrimeComponent in F:
                FPrime += [qPrime]
                break

    # generate deltaPrime
    deltaPrime = { }
    for q in QPrime:
        for a in sigma:
            destinations = []
            for qComponent in q:
                if DeltaEntry(qComponent, a) in delta:
                    destinations = list(set(destinations) | set(delta[DeltaEntry(qComponent, a)]))
            destinations.sort()
            epsilonChecks = list(destinations)
            checkedEpsilons = [ ]
            while len(epsilonChecks) > 0:
                checkState = epsilonChecks.pop(0)
                if checkState in checkedEpsilons:
                    continue
                checkedEpsilons += [ checkState ]
                if DeltaEntry(checkState, EPSILON) in delta:
                    destinations = list(set(destinations) | set(delta[DeltaEntry(checkState, EPSILON)]))
                    destinations.sort()
                    epsilonChecks += delta[DeltaEntry(checkState, EPSILON)]
            destinations.sort()    
            deltaPrime[ DeltaEntry(q, a) ] = destinations

    # let's determine which states, now, can be reached from the start
    canReachQPrime = [ ]
    for qIndex in range(len(QPrime)):
        canReachQPrime.append(False)
    canReachQPrime[QPrime.index(q0Prime)] = True
    canReachQPrime[QPrime.index([])] = True
    destinationsReachable = [ ]
    for a in sigma:
        if DeltaEntry(q0Prime, a) in deltaPrime:
            destinationsReachable.append(deltaPrime[DeltaEntry(q0Prime, a)])
    while len(destinationsReachable) > 0:
        destination = destinationsReachable.pop(0)
        if not (destination in QPrime):
            continue
        qPrimeIndex = QPrime.index(destination)
        if canReachQPrime[qPrimeIndex]:
            continue
        canReachQPrime[qPrimeIndex] = True
        for a in sigma:
            if DeltaEntry(destination, a) in deltaPrime:
                destinationsReachable.append(deltaPrime[DeltaEntry(destination, \
                                                                   a)])

    # Now, let's remove the states we can't reach
    unreachables = [ ]
    for qIndex in range(len(QPrime)):
        if not canReachQPrime[qIndex]:
            unreachables.append(QPrime[qIndex])
    for unreachable in unreachables:
        QPrime.remove(unreachable)
        for a in sigma:
            if DeltaEntry(unreachable, a) in deltaPrime:
                del deltaPrime[ DeltaEntry(unreachable, a) ]
        if unreachable in FPrime:
            FPrime.remove(unreachable)

    # print what we now have
    print("\n----------------  DFA  ------------------")
    printFa(QPrime, sigma, deltaPrime, q0Prime, FPrime)


#### OKAY. NOW YOU CAN MODIFY ANYTHING THAT COMES AFTER THIS. ####

# It isn't required to be a string (state names, etc), but I would HIGHLY recommend working
# only with strings, as I have set up in the example below

Q = ["q1", "q2", "q3"]     # the states of the NFA
sigma = ["a", "b"]
q0 = "q1"
F = ["q2"]               # the accept states.

delta = { } # This creates an empty dictionary

# to add a new transition, write as such:
#   delta[ DeltaEntry(CURRENT_STATE, INPUT) ] = DESTINATION_STATES
#
#   where CURRENT_STATE is the current state
#         INPUT is a valid value from sigma (or EPSILON -- all caps)
#         DESTINATION_STATES is a *list* of states
#
#   check out the examples below if you aren't familiar with Python

delta[ DeltaEntry("q1", EPSILON) ] = ["q2"]   
delta[ DeltaEntry("q1", "a") ] = ["q3"]
delta[ DeltaEntry("q2", "a") ] = ["q1"]
delta[ DeltaEntry("q3", "b") ] = ["q2", "q3"]
delta[ DeltaEntry("q3", "a") ] = ["q2"]

# now, let's run our program with what we set up!
convertToDfa(Q, sigma, delta, q0, F)
